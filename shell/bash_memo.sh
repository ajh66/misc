#!/usr/bin/bash -

#
# subshell
#
tar -cf - . | (cd /newdir; tar -xpf -)

#
# code block
#
cd /some/dir || {echo could not change to /some/dir ; echo you lose! ;}
# or
cd /some/dir || {
  echo could not change to /some/dir
  echo you lose!
}

#
# Remove all subdir (including its content) from current dir
#
find . -maxdepth 1 -mindepth 1 -type d -exec rm -rf {} \;

#
# Inside a super git repo run "git clean -xdf" in all its submodules
#
find . -maxdepth 1 -mindepth 1 -type d -exec git -C {} clean -xdf \;

#
# Remove all level 1 empty subdir
# See discussion https://stackoverflow.com/questions/9417967/how-to-list-empty-folders-in-linux
#
find . -maxdepth 1 -mindepth 1 -type d -empty -exec rmdir {} \;
find . -maxdepth 1 -mindepth 1 -type d -empty -delete
